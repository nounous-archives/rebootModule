#!/usr/bin/env python
"""vigileserver.py: UDP Server for vigile"""

import os, time, socket, smtplib
import SocketServer

HOST, PORT = "0.0.0.0", 1200
last_ts = {}

template = """From: %(from)s <root@%(from)s>
To: roots@crans.org
Subject: activity detected by %(from)s

%(lines)s

-- 
%(from)s
"""

recipient = os.getenv("EMAIL") or "roots@crans.org"

# Return host name when possible
def gethostname(s):
    try:
        return socket.gethostbyaddr(s)[0]
    except:
        return s

class VigileHandler(SocketServer.BaseRequestHandler):
    """Handler class for SocketServers"""
    def handle(self):
        """Handle the request"""
        data = self.request[0]
        socket = self.request[1]
        client = self.client_address[0]
        print ("%s wrote:" % client),
        print data,
        data = data.lower()
        now = time.time()
        lines = []

        if now-last_ts.get(client, 0) <= 600:
            # sent a mail less than 10 minutes ago, ignore
            return

        if data.startswith("d="):
            for i in (1, 2, 3):
                if data[1+i] == '1':
                    lines.append("activity detected on motion sensor %d" % i)

        if data.startswith("l="):
            # what do we do?
            pass

        if not lines:
            # nothing interesting
            return

        message = template % { 'from' : gethostname(client),
                               'lines': '\n'.join(lines) }
        mail = smtplib.SMTP('smtp.crans.org')
        mail.sendmail(recipient, recipient, message)
        last_ts[client] = now


if __name__ == "__main__":
    server = SocketServer.UDPServer((HOST, PORT), VigileHandler)
    server.serve_forever()
