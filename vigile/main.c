/*********************************************
 * vim:sw=4:ts=4:si:et
 * To use the above modeline in vim you must have "set modeline" in your .vimrc
 * Author: Guido Socher
 * Copyright: GPL V2
 * See http://www.gnu.org/licenses/gpl.html
 *
 * Ethernet remote device and sensor
 * UDP and HTTP interface 
        url looks like this http://baseurl/password/command
        or http://baseurl/password/
 *
 * Chip type           : Atmega88 or Atmega168 or Atmega328 with ENC28J60
 * Note: there is a version number in the text. Search for tuxgraphics
 *********************************************/
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "ip_arp_udp_tcp.h"
#include "enc28j60.h"
#include "timeout.h"
#include "avr_compat.h"
#include "net.h"
#include "module_helper.h"
/*#include "digicode.h"*/

// please modify the following two lines. mac and ip have to be unique
// in your local area network. You can not have the same numbers in
// two devices:
static uint8_t mymac[6];        // = {0x54,0x55,0x58,0x10,0x00,0x24};

// how did I get the mac addr? Translate the first 3 numbers into ascii is: TUX
static uint8_t myip[4];         // = {138,231,136,244};

//static uint8_t myip[4] = {192,168,255,100};
// listen port for tcp/www (max range 1-254)
#define MYWWWPORT 80
//
// listen port for udp
#define MYUDPPORT 1200

#define BUFFER_SIZE 550
static uint8_t buf[BUFFER_SIZE + 1];

// the password string (only the first 5 char checked), (only a-z,0-9,_ characters):
static char password[] = "passoir";     // must not be longer than 9 char

// 
uint8_t verify_password(char *str)
{
    // the first characters of the received string are
    // a simple password/cookie:
    if(strncmp(password, str, 5) == 0) {
        return (1);
    }
    return (0);
}

int main(void)
{


    uint16_t plen;
    uint8_t i = 0;

    uint8_t cmd_pos = 0;

    uint8_t payloadlen = 0;

    char str[30];

    char cmdval, *nxtVal, *curVal;

    uint8_t val4[4], val6[6];

    uint8_t detecteur0 = 0, detecteur1 = 0, detecteur2 = 0;

    int res1;

    // set the clock speed to "no pre-scaler" (8MHz with internal osc or 
    // full external speed)
    // set the clock prescaler. First write CLKPCE to enable setting of clock the
    // next four instructions.
    CLKPR = (1 << CLKPCE);      // change enable
    CLKPR = 0;                  // "no pre-scaler"
    _delay_loop_1(50);          // 12ms

    /* enable PD2/INT0, as input */
    DDRD &= ~(1 << DDD2);

    // Initialize TWI EEPROM
    initTWImem();

    // Read MAC and IP from EEPROM
    if((readMyMAC(mymac) != 0) || (readMyIP(myip) != 0)) {
        // Setting default value if the EEPROM contains illegal values
        myip[0] = 138;
        myip[1] = 231;
        myip[2] = 136;
        myip[3] = 246;
        mymac[0] = 0x43;
        mymac[1] = 0x52;
        mymac[2] = 0x40;
        mymac[3] = 0x4E;
        mymac[4] = 0x53;
        mymac[5] = 0x58;

        // Writting correct data into EEPROM
        writeMyMAC(mymac);
        writeMyIP(myip);
    }

    /*initialize enc28j60 */
    enc28j60Init(mymac);
    enc28j60clkout(2);          // change clkout from 6.25MHz to 12.5MHz
    _delay_loop_1(50);          // 12ms

    // LED
    /* enable PC1, LED as output */
    DDRC |= (1 << DDC1);

    /* set output to Vcc, LED off */
    PORTC |= (1 << PORTC1);

    // the buzzer on PB7
    DDRB |= (1 << DDB7);
    PORTB &= ~(1 << PORTB7);    // transistor off

    // the relay on PC2
    DDRC |= (1 << DDC2);
    PORTC &= ~(1 << PORTC2);     // power off

    /* Magjack leds configuration, see enc28j60 datasheet, page 11 */
    // LEDB=yellow LEDA=green
    //
    // 0x476 is PHLCON LEDA=links status, LEDB=receive/transmit
    // enc28j60PhyWrite(PHLCON,0b0000 0100 0111 01 10);
    enc28j60PhyWrite(PHLCON, 0x476);
    _delay_loop_1(50);          // 12ms

    /* set output to GND, orange LED on */
    PORTC &= ~(1 << PORTC1);
    i = 1;

    beep(30);
    _delay_ms(200);
    beep(30);

    // Set C0, C1, C2 to inputs (detectors, no pull-ups)
    C0_IN();
    C0_POFF();
    C1_IN();
    C1_POFF();
    C2_IN();
    C2_POFF();

    // Enable ADC6
    ADMUX |= ((0 << REFS1) | (1 << REFS0)) | (0x06);
    ADCSRA |= ((1 << ADEN) | (1 << ADSC) | (1 << ADATE));
    ADCSRA |= ((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0));
    ADCSRB |= 0x00;

    // Wait for first ADC conversion
    while ((ADCSRA & (1 << ADIF)) == 0);

    // Enable power
    PORTC |= (1 << PORTC2);

    //init the ethernet/ip layer:
    init_ip_arp_udp_tcp(mymac, myip, MYWWWPORT);

    while(1) {

        detecteur0 = (PIND & (1 << PIND0)) != 0;
        detecteur1 = (PIND & (1 << PIND1)) != 0;
        detecteur2 = (PIND & (1 << PIND5)) != 0;

        // get the next new packet:
        plen = enc28j60PacketReceive(BUFFER_SIZE, buf);

        /*plen will be unequal to zero if there is a valid 
         * packet (without crc error) */
        if(plen == 0) {
            continue;
        }

        // arp is broadcast if unknown but a host may also
        // verify the mac address by sending it to 
        // a unicast address.
        if(eth_type_is_arp_and_my_ip(buf, plen)) {
            make_arp_answer_from_request(buf);
            continue;
        }

        // check if ip packets are for us:
        if(eth_type_is_ip_and_my_ip(buf, plen) == 0) {
            continue;
        }
        // led----------
        if(i) {
            /* set output to Vcc, LED off */
            PORTC |= (1 << PORTC1);
            i = 0;
        }
        else {
            /* set output to GND, LED on */
            PORTC &= ~(1 << PORTC1);
            i = 1;
        }

        if(buf[IP_PROTO_P] == IP_PROTO_ICMP_V && buf[ICMP_TYPE_P] == ICMP_TYPE_ECHOREQUEST_V) {
            // a ping packet, let's send pong
            make_echo_reply_from_request(buf, plen);
            continue;
        }
        // udp start, we listen on udp port 1200=0x4B0
        if(buf[IP_PROTO_P] == IP_PROTO_UDP_V && buf[UDP_DST_PORT_H_P] == 4 && buf[UDP_DST_PORT_L_P] == 0xb0) {
            payloadlen = buf[UDP_LEN_L_P] - UDP_HEADER_LEN;
            // you must sent a string starting with v
            // e.g udpcom version 10.0.0.24
            if(verify_password((char *) &(buf[UDP_DATA_P]))) {
                // find the first comma which indicates 
                // the start of a command:
                cmd_pos = 0;
                while(cmd_pos < payloadlen) {
                    cmd_pos++;
                    if(buf[UDP_DATA_P + cmd_pos] == ',') {
                        cmd_pos++;      // put on start of cmd
                        break;
                    }
                }
                // a command is one char and a value. At
                // least 3 characters long. It has an '=' on
                // position 2:
                if(cmd_pos < 2 || cmd_pos > payloadlen - 3 || buf[UDP_DATA_P + cmd_pos + 1] != '=') {
                    strcpy(str, "e=no_cmd");
                    goto ANSWER;
                }
                // supported commands are
                // b=1 b=0 b=?             : buzzer
                // w=1 w=0 w=?             : power
                // m=XX:XX:XX:XX:XX:XX m=? : set/get EEPROM MAC
                // i=xxx.xxx.xxx.xxx   i=? : set/get EEPROM IP
                // a=?                     : get ADC value
                // d=?                     : get detector status
                if(buf[UDP_DATA_P + cmd_pos] == 'a') {
                    if ((ADCSRA & (1 << ADIF)) != 0)
                        sprintf(str, "a=%d", ADC);
                    else
                        strcpy(str, "a=NOK");
                    goto ANSWER;
                }
                if(buf[UDP_DATA_P + cmd_pos] == 'd') {
                    sprintf(str, "d=%d:%d:%d", detecteur0, detecteur1, detecteur2);
                    goto ANSWER;
                }
                if(buf[UDP_DATA_P + cmd_pos] == 'b') {
                    cmdval = buf[UDP_DATA_P + cmd_pos + 2];
                    if(cmdval == '1') {
                        PORTB |= (1 << PORTB7); // buzzer on
                        strcpy(str, "b=1");
                        goto ANSWER;
                    }
                    else if(cmdval == '0') {
                        PORTB &= ~(1 << PORTB7);        // buzzer off
                        strcpy(str, "b=0");
                        goto ANSWER;
                    }
                    else if(cmdval == '?') {
                        if(PORTB & (1 << PORTB7)) {
                            strcpy(str, "b=1");
                            goto ANSWER;
                        }
                        strcpy(str, "b=0");
                        goto ANSWER;
                    }
                }
                if(buf[UDP_DATA_P + cmd_pos] == 'w') {
                    cmdval = buf[UDP_DATA_P + cmd_pos + 2];
                    if(cmdval == '1') {
                        PORTC |= (1 << PORTC2); // power on
                        strcpy(str, "w=1");
                        goto ANSWER;
                    }
                    else if(cmdval == '0') {
                        PORTC &= ~(1 << PORTC2);        // power off
                        strcpy(str, "w=0");
                        goto ANSWER;
                    }
                    else if(cmdval == '?') {
                        if(PORTC & (1 << PORTC2)) {
                            strcpy(str, "w=1");
                            goto ANSWER;
                        }
                        strcpy(str, "w=0");
                        goto ANSWER;
                    }
                }
                if(buf[UDP_DATA_P + cmd_pos] == 'i') {
                    if(buf[UDP_DATA_P + cmd_pos + 2] == '?') {
                        readMyIP(val4);
                        sprintf(str, "IP=%d.%d.%d.%d", val4[0], val4[1], val4[2], val4[3]);
                        goto ANSWER;
                    }
                    nxtVal = curVal = &buf[UDP_DATA_P + cmd_pos + 2];
                    for(res1 = 0; res1 < 4; res1++) {
                        for(; *nxtVal != '.' && *nxtVal != '\0'; nxtVal++);
                        *nxtVal = '\0';
                        nxtVal++;
                        val4[res1] = atoi(curVal);
                        curVal = nxtVal;
                    }
                    writeMyIP(val4);
                    sprintf(str, "IP=%d.%d.%d.%d", val4[0], val4[1], val4[2], val4[3]);
                    goto ANSWER;
                }
                if(buf[UDP_DATA_P + cmd_pos] == 'm') {
                    if(buf[UDP_DATA_P + cmd_pos + 2] == '?') {
                        readMyMAC(val6);
                        sprintf(str, "MAC=%02X:%02X:%02X:%02X:%02X:%02X", val6[0], val6[1], val6[2], val6[3], val6[4], val6[5]);
                        goto ANSWER;
                    }
                    sscanf(&buf[UDP_DATA_P + cmd_pos + 2], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                           &val6[0], &val6[1], &val6[2], &val6[3], &val6[4], &val6[5]);
                    writeMyMAC(val6);
                    sprintf(str, "MAC=%02X:%02X:%02X:%02X:%02X:%02X", val6[0], val6[1], val6[2], val6[3], val6[4], val6[5]);
                    goto ANSWER;
                }

                strcpy(str, "e=no_such_cmd");
                goto ANSWER;
            }
            strcpy(str, "e=invalid_pw");
          ANSWER:
            make_udp_reply_from_request(buf, str, strlen(str), MYUDPPORT);
        }
    }
    return (0);
}
