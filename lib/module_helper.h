// Function and macros to ease the use of the Reboot Module interface
// X. Lagorce - 2009

#ifndef H__REBOOTMODULE
#define H__REBOOTMODULE

#define F_CPU 12500000UL

#include<util/delay.h>
#include<util/twi.h>
#include<avr/eeprom.h>
#include<avr/interrupt.h>

// Constants
// TWI address for 24Cxx EEPROM :
#define TWI_SLA_24CXX	0xa0    /* A2 A1 A0 = 0 0 0 */
// Maximal number of iterations to wait for a device to respond for a
// selection
#define MAX_ITER	200
// EEPROM Page size
#define PAGE_SIZE       64
// USART parameters
#define BAUD    9600
#define UBRR    F_CPU/16/BAUD-1

// EEPROM addresses
// Boot Flags
#define EE_ADD_BOOT_FLAGS 0x00
#define EE_LEN_BOOT_FLAGS 1

// MAC Address + Checksum
#define EE_ADD_MAC (EE_ADD_BOOT_FLAGS + EE_LEN_BOOT_FLAGS + 1)
#define EE_LEN_MAC 7

// IP Address + Checksum
#define EE_ADD_IP (EE_ADD_MAC + EE_LEN_MAC + 1)
#define EE_LEN_IP 5

// Other Mac Address + Checksum
#define EE_ADD_MAC2 (EE_ADD_IP + EE_LEN_IP + 1)
#define EE_LEN_MAC2 7

// Other IP Address + Checksum
#define EE_ADD_IP2 (EE_ADD_MAC2 + EE_LEN_MAC2 + 1)
#define EE_LEN_IP2 5

// First free address
#define EE_ADD_FREE (EE_ADD_IP2 + EE_LEN_IP2 + 1)

// Configuration macros
#define BUZZER_OUT() DDRB |= (1<<DDB7)
#define LED_OUT()    DDRC |= (1<<DDC1)
#define POWER_OUT()  DDRC |= (1<<DDC2)

#define C0_OUT()     DDRD |= (1<<DDD0)
#define C1_OUT()     DDRD |= (1<<DDD1)
#define C2_OUT()     DDRD |= (1<<DDD5)
#define C3_OUT()     DDRD |= (1<<DDD6)
#define C4_OUT()     DDRD |= (1<<DDD7)
#define C5_OUT()     DDRB |= (1<<DDB0)
#define C6_OUT()     DDRB |= (1<<DDB1)

#define C0_IN()      DDRD &= ~(1<<DDD0)
#define C1_IN()      DDRD &= ~(1<<DDD1)
#define C2_IN()      DDRD &= ~(1<<DDD5)
#define C3_IN()      DDRD &= ~(1<<DDD6)
#define C4_IN()      DDRD &= ~(1<<DDD7)
#define C5_IN()      DDRB &= ~(1<<DDB0)
#define C6_IN()      DDRB &= ~(1<<DDB1)

#define C0_PON()     PORTD |= (1<<PORTD0)
#define C1_PON()     PORTD |= (1<<PORTD1)
#define C2_PON()     PORTD |= (1<<PORTD5)
#define C3_PON()     PORTD |= (1<<PORTD6)
#define C4_PON()     PORTD |= (1<<PORTD7)
#define C5_PON()     PORTB |= (1<<PORTB0)
#define C6_PON()     PORTB |= (1<<PORTB1)

#define C0_POFF()    PORTD &= ~(1<<PORTD0)
#define C1_POFF()    PORTD &= ~(1<<PORTD1)
#define C2_POFF()    PORTD &= ~(1<<PORTD5)
#define C3_POFF()    PORTD &= ~(1<<PORTD6)
#define C4_POFF()    PORTD &= ~(1<<PORTD7)
#define C5_POFF()    PORTB &= ~(1<<PORTB0)
#define C6_POFF()    PORTB &= ~(1<<PORTB1)

#define MEMWP_ON()   PORTC |= (1<<PORTC3)
#define MEMWP_OFF()  PORTC &= ~(1<<PORTC3)

// Helper macros
#define BUZZER_ON()  PORTB |=  (1<<PORTB7)
#define BUZZER_OFF() PORTB &= ~(1<<PORTB7)

#define LED_ON()     PORTC &= ~(1<<PORTC1)
#define LED_OFF()    PORTC |=  (1<<PORTC1)

#define POWER_ON()   PORTC |=  (1<<PORTC2)
#define POWER_OFF()  PORTC &= ~(1<<PORTC2)

#define C0_ON()      PORTD |=  (1<<PORTD0)
#define C1_ON()      PORTD |=  (1<<PORTD1)
#define C2_ON()      PORTD |=  (1<<PORTD5)
#define C3_ON()      PORTD |=  (1<<PORTD6)
#define C4_ON()      PORTD |=  (1<<PORTD7)
#define C5_ON()      PORTB |=  (1<<PORTB0)
#define C6_ON()      PORTB |=  (1<<PORTB1)

#define C0_OFF()     PORTD &= ~(1<<PORTD0)
#define C1_OFF()     PORTD &= ~(1<<PORTD1)
#define C2_OFF()     PORTD &= ~(1<<PORTD5)
#define C3_OFF()     PORTD &= ~(1<<PORTD6)
#define C4_OFF()     PORTD &= ~(1<<PORTD7)
#define C5_OFF()     PORTB &= ~(1<<PORTB0)
#define C6_OFF()     PORTB &= ~(1<<PORTB1)


// Helper functions
//inline void beep(double duration);
inline void beep_10(void);
inline void beep_30(void);
inline void beep_100(void);
inline void beep_500(void);

inline void blinkON(double duration);
inline void blinkOFF(double duration);

inline void Cin(unsigned char pullUp);

inline void Cout(void);

inline unsigned char getC(void);

inline void setC(unsigned char value, unsigned char mask);

int readMyIP(uint8_t * myIP);
int writeMyIP(uint8_t * myIP);
int readMyMAC(uint8_t * myMAC);
int writeMyMAC(uint8_t * myMAC);

int readOtherIP(uint8_t * myIP);
int writeOtherIP(uint8_t * myIP);
int readOtherMAC(uint8_t * myMAC);
int writeOtherMAC(uint8_t * myMAC);

void initTWImem(void);

void twiMemSoftReset(void);

int twiMemReadBytes(uint16_t eeaddr, int len, uint8_t * buf);

int twiMemWritePage(uint16_t eeaddr, int len, uint8_t * buf);

int twiMemWriteBytes(uint16_t eeaddr, int len, uint8_t * buf);

void initUSART(void);

#endif
