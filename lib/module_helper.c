// Function and macros to ease the use of the Reboot Module interface
// X. Lagorce - 2009

#include "module_helper.h"

// Saved TWI status register, for error messages only.
uint8_t twst;

/*inline void beep(double duration)
{
    BUZZER_ON();
    _delay_ms(duration);
    BUZZER_OFF();
    }*/

inline void beep_10(void)
{
    BUZZER_ON();
    _delay_ms(10);
    BUZZER_OFF();
}
inline void beep_30(void)
{
    BUZZER_ON();
    _delay_ms(30);
    BUZZER_OFF();
}
inline void beep_100(void)
{
    BUZZER_ON();
    _delay_ms(100);
    BUZZER_OFF();
}
inline void beep_500(void)
{
    BUZZER_ON();
    _delay_ms(500);
    BUZZER_OFF();
}

inline void blinkON(double duration)
{
    LED_ON();
    _delay_ms(duration);
    LED_OFF();
}

inline void blinkOFF(double duration)
{
    LED_OFF();
    _delay_ms(duration);
    LED_ON();
}

inline void Cin(unsigned char pullUp)
{
    DDRD &= ~0xE3;
    DDRB &= ~0x03;

    if(pullUp) {
        PORTD |= 0xE3;
        PORTB |= 0x03;
    }
    else {
        PORTD &= ~0xE3;
        PORTB &= ~0x03;
    }
}

inline void Cout(void)
{
    DDRD |= 0xE3;
    DDRB |= 0x03;
}

inline unsigned char getC(void)
{
    return (PORTD & 0x03) | ((PORTD & 0xE0) >> 3) | ((PORTB & 0x03) << 5);
}

inline void setC(unsigned char value, unsigned char mask)
{
    unsigned char port, final;

    mask = mask & 0x7F;

    port = getC();
    final = (port & (~mask)) | (value & mask);

    PORTD = (PORTD & 0x1C) | (final & 0x3) | ((final & 0x1E) << 3);
    PORTB = (PORTB & 0xFC) | ((final & 0x60) >> 5);
}

int readMyIP(uint8_t * myIP)
{
    uint8_t buf[EE_LEN_IP], chk;

    if(twiMemReadBytes(EE_ADD_IP, EE_LEN_IP, buf) != EE_LEN_IP) {
        return -1;
    }
    chk = buf[0] ^ buf[1] ^ buf[2] ^ buf[3];
    if(chk != buf[4])
        return 1;

    myIP[0] = buf[0];
    myIP[1] = buf[1];
    myIP[2] = buf[2];
    myIP[3] = buf[3];

    return 0;
}

int writeMyIP(uint8_t * myIP)
{
    uint8_t buf[EE_LEN_IP];

    buf[0] = myIP[0];
    buf[1] = myIP[1];
    buf[2] = myIP[2];
    buf[3] = myIP[3];
    buf[4] = buf[0] ^ buf[1] ^ buf[2] ^ buf[3];

    //MEMWP_OFF();
    if(twiMemWriteBytes(EE_ADD_IP, EE_LEN_IP, buf) != EE_LEN_IP) {
        //MEMWP_ON();
        return -1;
    }

    //MEMWP_ON();
    return 0;
}

int readMyMAC(uint8_t * myMAC)
{
    uint8_t buf[EE_LEN_MAC], chk;

    if(twiMemReadBytes(EE_ADD_MAC, EE_LEN_MAC, buf) != EE_LEN_MAC) {
        return -1;
    }
    chk = buf[0] ^ buf[1] ^ buf[2] ^ buf[3] ^ buf[4] ^ buf[5];
    if(chk != buf[6])
        return 1;

    myMAC[0] = buf[0];
    myMAC[1] = buf[1];
    myMAC[2] = buf[2];
    myMAC[3] = buf[3];
    myMAC[4] = buf[4];
    myMAC[5] = buf[5];

    return 0;
}

int writeMyMAC(uint8_t * myMAC)
{
    uint8_t buf[EE_LEN_MAC];

    buf[0] = myMAC[0];
    buf[1] = myMAC[1];
    buf[2] = myMAC[2];
    buf[3] = myMAC[3];
    buf[4] = myMAC[4];
    buf[5] = myMAC[5];
    buf[6] = buf[0] ^ buf[1] ^ buf[2] ^ buf[3] ^ buf[4] ^ buf[5];

    //MEMWP_OFF();
    if(twiMemWriteBytes(EE_ADD_MAC, EE_LEN_MAC, buf) != EE_LEN_MAC) {
        //MEMWP_ON();
        return -1;
    }

    //MEMWP_ON();
    return 0;
}

int readOtherIP(uint8_t * myIP)
{
    uint8_t buf[EE_LEN_IP2], chk;

    if(twiMemReadBytes(EE_ADD_IP2, EE_LEN_IP2, buf) != EE_LEN_IP2) {
        return -1;
    }
    chk = buf[0] ^ buf[1] ^ buf[2] ^ buf[3];
    if(chk != buf[4])
        return 1;

    myIP[0] = buf[0];
    myIP[1] = buf[1];
    myIP[2] = buf[2];
    myIP[3] = buf[3];

    return 0;
}

int writeOtherIP(uint8_t * myIP)
{
    uint8_t buf[EE_LEN_IP2];

    buf[0] = myIP[0];
    buf[1] = myIP[1];
    buf[2] = myIP[2];
    buf[3] = myIP[3];
    buf[4] = buf[0] ^ buf[1] ^ buf[2] ^ buf[3];

    //MEMWP_OFF();
    if(twiMemWriteBytes(EE_ADD_IP2, EE_LEN_IP2, buf) != EE_LEN_IP2) {
        //MEMWP_ON();
        return -1;
    }

    //MEMWP_ON();
    return 0;
}

int readOtherMAC(uint8_t * myMAC)
{
    uint8_t buf[EE_LEN_MAC2], chk;

    if(twiMemReadBytes(EE_ADD_MAC2, EE_LEN_MAC2, buf) != EE_LEN_MAC2) {
        return -1;
    }
    chk = buf[0] ^ buf[1] ^ buf[2] ^ buf[3] ^ buf[4] ^ buf[5];
    if(chk != buf[6])
        return 1;

    myMAC[0] = buf[0];
    myMAC[1] = buf[1];
    myMAC[2] = buf[2];
    myMAC[3] = buf[3];
    myMAC[4] = buf[4];
    myMAC[5] = buf[5];

    return 0;
}

int writeOtherMAC(uint8_t * myMAC)
{
    uint8_t buf[EE_LEN_MAC2];

    buf[0] = myMAC[0];
    buf[1] = myMAC[1];
    buf[2] = myMAC[2];
    buf[3] = myMAC[3];
    buf[4] = myMAC[4];
    buf[5] = myMAC[5];
    buf[6] = buf[0] ^ buf[1] ^ buf[2] ^ buf[3] ^ buf[4] ^ buf[5];

    //MEMWP_OFF();
    if(twiMemWriteBytes(EE_ADD_MAC2, EE_LEN_MAC2, buf) != EE_LEN_MAC2) {
        //MEMWP_ON();
        return -1;
    }

    //MEMWP_ON();
    return 0;
}


// Initialize TWI EEPROM module
void initTWImem(void)
{
    // Initialize TWI clock: 100 kHz clock, TWPS = 0 => prescaler = 1
    TWSR = 0;
    TWBR = (F_CPU / 100000UL - 16) / 2;
    // EEPROM WP defined as output
    DDRC |= (1 << DDC3);
    // WP enabled
    MEMWP_OFF();
}

// Realize a soft reset of the TWI EEPROM
void twiMemSoftReset(void)
{

}

// Read "len" bytes from EEPROM starting at "eeaddr" into "buf".
int twiMemReadBytes(uint16_t eeaddr, int len, uint8_t * buf)
{
    uint8_t sla, twcr, n = 0;

    int rv = 0;

    // patch high bits of EEPROM address into SLA
    sla = TWI_SLA_24CXX;        // | (((eeaddr >> 8) & 0x07) << 1);

    // First cycle: master transmitter mode
  restart:
    if(n++ >= MAX_ITER)
        return -1;
  begin:

    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); // send start condition
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_REP_START:       // OK, but should not happen
      case TW_START:
          break;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          return -1;            // error: not in start condition
          // NB: do /not/ send stop condition
    }

    // send SLA+W
    TWDR = sla | TW_WRITE;
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_SLA_ACK:
          break;

      case TW_MT_SLA_NACK:     // nack during select: device busy writing

          goto restart;

      case TW_MT_ARB_LOST:     // re-arbitrate
          goto begin;

      default:
          goto error;           // must send stop condition
    }

    TWDR = (eeaddr & 0xFF00) >> 8;      // high 8 bits of addr
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_DATA_ACK:
          break;

      case TW_MT_DATA_NACK:
          goto quit;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          goto error;           // must send stop condition
    }

    TWDR = eeaddr & 0x00FF;     // low 8 bits of addr
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_DATA_ACK:
          break;

      case TW_MT_DATA_NACK:
          goto quit;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          goto error;           // must send stop condition
    }


    // Next cycle(s): master receiver mode
    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); // send (rep.) start condition
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_START:           // OK, but should not happen
      case TW_REP_START:
          break;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          goto error;
    }

    // send SLA+R
    TWDR = sla | TW_READ;
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MR_SLA_ACK:
          break;

      case TW_MR_SLA_NACK:
          goto quit;

      case TW_MR_ARB_LOST:
          goto begin;

      default:
          goto error;
    }

    for(twcr = _BV(TWINT) | _BV(TWEN) | _BV(TWEA); len > 0; len--) {
        if(len == 1)
            twcr = _BV(TWINT) | _BV(TWEN);      // send NAK this time
        TWCR = twcr;            // clear int to start transmission
        while((TWCR & _BV(TWINT)) == 0);        // wait for transmission
        switch ((twst = TW_STATUS)) {
          case TW_MR_DATA_NACK:
              len = 0;          // force end of loop
              // FALLTHROUGH
          case TW_MR_DATA_ACK:
              *buf++ = TWDR;
              rv++;
              break;

          default:
              goto error;
        }
    }
  quit:
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN); // send stop condition

    return rv;

  error:
    rv = -1;
    goto quit;
}

// Write "len" bytes into EEPROM starting at "eeaddr" from "buf".
int twiMemWritePage(uint16_t eeaddr, int len, uint8_t * buf)
{
    uint8_t sla, n = 0;

    int rv = 0;

    uint16_t endaddr;

    if(eeaddr + len < (eeaddr | (PAGE_SIZE - 1)))
        endaddr = eeaddr + len;
    else
        endaddr = (eeaddr | (PAGE_SIZE - 1)) + 1;
    len = endaddr - eeaddr;

    // patch high bits of EEPROM address into SLA
    sla = TWI_SLA_24CXX;        // | (((eeaddr >> 8) & 0x07) << 1);

  restart:
    if(n++ >= MAX_ITER)
        return -1;
  begin:

    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); // send start condition
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_REP_START:       // OK, but should not happen
      case TW_START:
          break;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          return -1;            // error: not in start condition
          // NB: do /not/ send stop condition
    }

    /* send SLA+W */
    TWDR = sla | TW_WRITE;
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_SLA_ACK:
          break;

      case TW_MT_SLA_NACK:     // nack during select: device busy writing
          goto restart;

      case TW_MT_ARB_LOST:     // re-arbitrate
          goto begin;

      default:
          goto error;           // must send stop condition
    }

    TWDR = (eeaddr & 0xFF00) >> 8;      // high 8 bits of addr
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_DATA_ACK:
          break;

      case TW_MT_DATA_NACK:
          goto quit;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          goto error;           // must send stop condition
    }

    TWDR = eeaddr & 0x00FF;     // low 8 bits of addr
    TWCR = _BV(TWINT) | _BV(TWEN);      // clear interrupt to start transmission
    while((TWCR & _BV(TWINT)) == 0);    // wait for transmission
    switch ((twst = TW_STATUS)) {
      case TW_MT_DATA_ACK:
          break;

      case TW_MT_DATA_NACK:
          goto quit;

      case TW_MT_ARB_LOST:
          goto begin;

      default:
          goto error;           // must send stop condition
    }

    for(; len > 0; len--) {
        TWDR = *buf++;
        TWCR = _BV(TWINT) | _BV(TWEN);  // start transmission
        while((TWCR & _BV(TWINT)) == 0);        // wait for transmission
        switch ((twst = TW_STATUS)) {
          case TW_MT_DATA_NACK:
              goto error;       // device write protected

          case TW_MT_DATA_ACK:
              rv++;
              break;

          default:
              goto error;
        }
    }
  quit:
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN); // send stop condition

    return rv;

  error:
    rv = -1;
    goto quit;
}

/*
 * Wrapper around twiMemWritePage() that repeats calling this
 * function until either an error has been returned, or all bytes
 * have been written.
 */
int twiMemWriteBytes(uint16_t eeaddr, int len, uint8_t * buf)
{
    int rv, total;

    total = 0;
    do {
        rv = twiMemWritePage(eeaddr, len, buf);

        if(rv == -1)
            return -1;
        eeaddr += rv;
        len -= rv;
        buf += rv;
        total += rv;
    }
    while(len > 0);

    return total;
}

// Return last TWI error
uint8_t twiError(void)
{

    return twst;
}

// Initialization of the serial interface
void initUSART()
{
    // Turn on the transmission and reception circuitry
    UBRR0 = UBRR;                             // Set Baudrate
    UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);  // Character Size 8 bit
    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);    // Receiver and Transmitter Enable
}
