# Reboot Module main Makefile.
# Authors: Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#          Xavier Lagorce <Xavier.Lagorce@crans.org>
#
# Inspired from Guido Socher's original Makefile

# /!\ Before building to a new MCU, make clean /!\

# to build for Atmega368p:
# make MCU=atmega328p <target>
# for Atmega168:
# make MCU=atmega168 <target>

# to program with avr910 programmer:
# make PROGTYPE=avr910 PROGARGS="-P /dev/ttyUSB0" <target>
# with usbtiny:
# make PROGTYPE=usbtiny PROGARGS= <target>

# Mix and match to your taste !


# Chip type
MCU=atmega168
DUDECPUTYPE=m168
# Flash commands
LOADCMD=avrdude
# Default programmer
PROGTYPE=avr910
PROGARGS=-P /dev/ttyUSB0
LOADARG=-F -p $(DUDECPUTYPE) -c $(PROGTYPE) $(PROGARGS) -e
FLASHARG=-U flash:w:

ifeq ($(MCU),atmega168)
	DUDECPUTYPE=m168
	FUSEARG=-U lfuse:w:0x60:m -U hfuse:w:0xDF:m -U efuse:w:0x01:m
else
	ifeq ($(MCU),atmega328p)
		DUDECPUTYPE=m328p
		FUSEARG=-U lfuse:w:0x60:m -U hfuse:w:0xD9:m -U efuse:w:0x07:m
	else
		@echo Platform not found (set MCU)
		@exit 2
	endif
endif

# Toolchain
CC=avr-gcc
AS=avr-as
OBJCOPY=avr-objcopy

# CFLAGS
# optimize for size:
CFLAGS=-g -mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues
# Disable unused functions
CFLAGS+=-ffunction-sections -fdata-sections
override LDFLAGS = -Wl,--relax,--gc-sections
# Inlined small functions take space; optimize on whole program
CFLAGS+= -fno-inline-small-functions --combine -fwhole-program
# Libraries
CFLAGS+=-lm
# Includes
CFLAGS+=-Ilib

ASFLAGS=-mmcu=$(MCU)

# Build Targets
.PHONY: all
all: build/digicode.hex build/rfid_proto.hex
	@echo "done"

# To find deps for a target, use `gcc -MM` on the main.c file, then filter .h files with no .c files

build/digicode.out: digicode/main.c lib/ip_arp_udp_tcp.c lib/enc28j60.c lib/module_helper.c digicode/digicode.c
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$*.map $^

build/rfid_proto.out: rfid_proto/main.c rfid_proto/rfid.c lib/ip_arp_udp_tcp.c lib/enc28j60.c lib/module_helper.c
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$*.map $^

build/vigile.out: vigile/main.c lib/ip_arp_udp_tcp.c lib/enc28j60.c lib/module_helper.c
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$*.map $^

build/firmware.out: firmware/main.c lib/ip_arp_udp_tcp.c lib/enc28j60.c lib/module_helper.c
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$*.map $^

%.hex: %.out
	$(OBJCOPY) -R .eeprom -O ihex $< $@
	avr-size $<
	@echo " "
	@echo "Expl.: data=initialized data, bss=uninitialized data, text=code"
	@echo " "


# Load .hex file to target.
.PHONY: %.load
%.load: %.hex
	$(LOADCMD) $(LOADARG) $(FLASHARG)$<

# Fuse check
rdfuses:
	$(LOADCMD) $(LOADARG) -q -U lfuse:r:build/low.txt:s -U hfuse:r:build/high.txt:s -U efuse:r:build/ext.txt:s
	@echo high
	@cat build/high.txt
	@echo low
	@cat build/low.txt
	@echo extended
	@cat build/ext.txt
	@rm build/high.txt build/low.txt build/ext.txt
fuse:
	@echo "warning: run this command only if you have an external clock on xtal1"
	@echo "The is the last chance to stop. Press crtl-C to abort"
	@sleep 5
	$(LOADCMD) $(LOADARG) $(FUSEARG)


clean:
	rm -f build/*
