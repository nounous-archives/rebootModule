// RFID module helper functions

#include "rfid.h"

// Decode the hex byte as a real uint8_t
uint8_t decode_hex_byte(char byte) {
    if (byte >= '0' && byte <= '9')
        return byte - '0';
    if (byte >= 'A' && byte <= 'F')
        return byte - 'A' + 10;
    if (byte >= 'a' && byte <= 'f')
        return byte - 'a' + 10;
    return 0;
}

// Decode the first two ascii bytes as a real byte.
uint8_t decode_two_hex_bytes(char *str) {
    uint8_t tmp;

    tmp = decode_hex_byte(str[0]) << 4;
    return tmp + decode_hex_byte(str[1]);
}

// Convert a code from ascii to a series of bytes
void convert_code(char *ascii_code, uint8_t *dest_code) {
    uint8_t j;

    for (j=0; j < 6; j++) {
        dest_code[j] = decode_two_hex_bytes(ascii_code + (2*j));
    }
}

// Validate the code's checksum
// Returns 0 if the checksum is valid
uint8_t validate_checksum(uint8_t *code) {
    uint8_t j, result;

    result = code[0];
    for (j=1; j<6; j++) {
        result ^= code[j];
    }
    return result;
}

// Read the number of codes available
uint8_t read_ncodes(void) {
    uint8_t ncodes;

    if (twiMemReadBytes(EE_ADD_NCODES, EE_LEN_NCODES, &ncodes) != EE_LEN_NCODES) {
        return 0;
    }
    return ncodes;
}

// Write a new number of codes available
uint8_t write_ncodes(uint8_t ncodes) {
    return twiMemWriteBytes(EE_ADD_NCODES, EE_LEN_NCODES, &ncodes);
}

// Read the code at address ncode. Return the number of bytes read
uint8_t read_code(uint8_t ncode, uint8_t *dst) {
    if (ncode < read_ncodes())
        return twiMemReadBytes(EE_ADD_CODE + (EE_LEN_CODE * ncode), EE_LEN_CODE, dst);
    else
        return 0;
}

// Write the code at address ncode. Do all the necessary checks to avoid empty spaces.
uint8_t write_code(uint8_t ncode, uint8_t *src) {
    int nwritten;
    if (ncode > read_ncodes())
        return 0;

    nwritten = twiMemWriteBytes(EE_ADD_CODE + (ncode * EE_LEN_CODE), EE_LEN_CODE, src);

    if (nwritten == EE_LEN_CODE) {
        if (ncode == read_ncodes()) {
            write_ncodes(ncode + 1);
        }
    }
    return nwritten;
}

void delete_code(uint8_t ncode) {
    uint8_t curcode[EE_LEN_CODE];
    uint8_t j, ncodes;
    ncodes = read_ncodes();

    if (ncode >= ncodes) {
        return;
    }

    for (j = ncode; j < ncodes; j++) {
        read_code(j+1, curcode);
        write_code(j, curcode);
    }
    write_ncodes(ncodes - 1);
}

// Compare 2 codes. Return 0 if codes are identical.
uint8_t compare_codes(uint8_t *code1, uint8_t *code2) {
    uint8_t n;

    for (n=0; n < EE_LEN_CODE; n++) {
        if (code1[n] != code2[n])
            return 1;
    }
    return 0;
}

// Validate the code
// We assume the checksum is correct
uint8_t validate_code(uint8_t *code) {
    uint8_t n_codes;
    uint8_t j;

    uint8_t cur_code[5];

    n_codes = read_ncodes();

    for (j = 0; j < n_codes; j++) {
        if (read_code(j, cur_code) == EE_LEN_CODE) {
            if (compare_codes(cur_code, code) == 0)
                return 0;
        }
    }

    return 1;
}
