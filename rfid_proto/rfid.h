// RFID Module helper function headers

#include <stdint.h>

#include "module_helper.h"

// constants
#define EE_ADD_NCODES EE_ADD_FREE
#define EE_LEN_NCODES 1

#define EE_ADD_CODE (EE_ADD_NCODES + EE_LEN_NCODES + 1)
#define EE_LEN_CODE 5

// Decode the hex byte as a real uint8_t
uint8_t decode_hex_byte(char byte);

// Decode the first two ascii bytes as a real byte.
uint8_t decode_two_hex_bytes(char *str);

// Convert a code from ascii to a series of bytes
void convert_code(char *ascii_code, uint8_t *dest_code);

// Validate the checksum
uint8_t validate_checksum(uint8_t *code);

// Read the number of codes available
uint8_t read_ncodes(void);

// Write a new number of codes available
uint8_t write_ncodes(uint8_t ncodes);

// Read the code at address ncode into dst
uint8_t read_code(uint8_t ncode, uint8_t *dst);

// Write the code at address ncode. Do all the necessary checks to avoid empty spaces.
uint8_t write_code(uint8_t ncode, uint8_t *src);

// Delete the code at address ncode.
void delete_code(uint8_t ncode);

// Validate the code
uint8_t validate_code(uint8_t *code);
