/*
 * Fonctions helper pour le digicode
 *
 * Auteurs: Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
 *          Xavier Lagorce <Xavier.Lagorce@crans.org>
 * Licence: GPL v.2
 */

#include <util/delay_basic.h>

#include "module_helper.h"

/*
 * Pin assignment
 * Connector -> Keyboard
 * C0           1         ROW A
 * C1           2         ROW B
 * C2           3         COL 1
 * C3           4         COL 2
 * C4           5         COL 3
 * C5           7         ROW D
 * C6           8         ROW C
 */

#define ROW_A (1<<DDD0)
#define ROW_B (1<<DDD1)
#define ROW_C (1<<DDB1)
#define ROW_D (1<<DDB0)
#define COL_1 (1<<DDD5)
#define COL_2 (1<<DDD6)
#define COL_3 (1<<DDD7)

// Lecture du clavier
uint8_t read_keyboard(void);

// Décodage d'un code clavier
uint8_t decode_key(uint8_t keysym);

// Open the door
void open_door(void);
