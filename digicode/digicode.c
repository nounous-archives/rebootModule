/*
 * digicode.c: Fonctions pour le digicode
 *
 * Auteur: Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
 *         Xavier Lagorce <Xavier.Lagorce@crans.org>
 * Licence: GPL v.2
 */

#include "digicode.h"

uint8_t read_keyboard(void) {

    uint8_t read = 0;

    // Set all columns to input, activate pull-ups
    DDRD &= ~(COL_1 | COL_2 | COL_3);
    PORTD |= COL_1 | COL_2 | COL_3;

    // Set all rows to output
    DDRD |= ROW_A | ROW_B;
    DDRB |= ROW_C | ROW_D;

    // Deactivate all rows
    PORTD &= ~(ROW_A | ROW_B);
    PORTB &= ~(ROW_C | ROW_D);

    _delay_us(25);

    // Read all cols
    read = ((((PIND & COL_1) ? 0 : 1)) |
            (((PIND & COL_2) ? 0 : 2)) |
            (((PIND & COL_3) ? 0 : 4)));

    // Set rows to input
    DDRD &= ~(ROW_A | ROW_B);
    DDRB &= ~(ROW_C | ROW_D);
    PORTD |= ROW_A | ROW_B;
    PORTB |= ROW_C | ROW_D;

    // Set columns to output
    DDRD |= COL_1 | COL_2 | COL_3;
    PORTD &= ~(COL_1 | COL_2 | COL_3);

    _delay_us(25);

    read |= (((PINB & ROW_D) ? 0 : 0x80)) |
            (((PINB & ROW_C) ? 0 : 0x40)) |
            (((PIND & ROW_B) ? 0 : 0x20)) |
            (((PIND & ROW_A) ? 0 : 0x10));


    // Set all keyboard pins to inputs
    DDRD &= ~(ROW_A | ROW_B | COL_1 | COL_2 | COL_3);
    DDRB &= ~(ROW_C | ROW_D);
    PORTD |= ROW_A | ROW_B | COL_1 | COL_2 | COL_3;
    PORTB |= ROW_C | ROW_D;

    if ((((read & 0x80) >> 7) + ((read & 0x40) >> 6)
         + ((read & 0x20) >> 5) + ((read & 0x10) >> 4)) != 1)
        return 0;

    if ((((read & 0x08) >> 3) + ((read & 0x04) >> 2)
         + ((read & 0x02) >> 1) + ((read & 0x01) >> 0)) != 1)
        return 0;

    return read;
}

uint8_t decode_key(uint8_t keysym)
{
    uint8_t ret = 0;
    if ((keysym & 0x10) != 0) { // Row A
        if ((keysym & 0x01) != 0) {
            ret = '1';
            goto end;
        }
        if ((keysym & 0x02) != 0) {
            ret = '2';
            goto end;
        }
        if ((keysym & 0x04) != 0) {
            ret = '3';
            goto end;
        }
        goto end;
    }
    if ((keysym & 0x20) != 0) { // Row B
        if ((keysym & 0x01) != 0) {
            ret = '4';
            goto end;
        }
        if ((keysym & 0x02) != 0) {
            ret = '5';
            goto end;
        }
        if ((keysym & 0x04) != 0) {
            ret = '6';
            goto end;
        }
        goto end;
    }
    if ((keysym & 0x40) != 0) { // Row C
        if ((keysym & 0x01) != 0) {
            ret = '7';
            goto end;
        }
        if ((keysym & 0x02) != 0) {
            ret = '8';
            goto end;
        }
        if ((keysym & 0x04) != 0) {
            ret = '9';
            goto end;
        }
        goto end;
    }
    if ((keysym & 0x80) != 0) { // Row D
        if ((keysym & 0x01) != 0) {
            ret = '*';
            goto end;
        }
        if ((keysym & 0x02) != 0) {
            ret = '0';
            goto end;
        }
        if ((keysym & 0x04) != 0) {
            ret = '#';
            goto end;
        }
        goto end;
    }
    end:
    return ret;
}

// Open the door
void open_door(void) {
  POWER_ON();
  beep_500();
  _delay_ms(3500);
  POWER_OFF();
}
