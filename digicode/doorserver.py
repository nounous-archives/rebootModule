#!/usr/bin/env python
"""doorserver.py: UDP Server for door opening requests"""

import os
import SocketServer

CODES = "codes"
HOST, PORT = "zamok.crans.org", 1200

def check_code(data):
    """Check the given code against the available codes list."""
    path = os.path.join(CODES, data)
    if os.path.exists(path):
        contents = open(path).read()
        os.remove(path)
        return True, contents
    return False, ""

class VigileHandler(SocketServer.BaseRequestHandler):
    """Handler class for SocketServers, answering to door requests"""
    def handle(self):
        """Handle the request the door sent us"""
        data = self.request[0]
        socket = self.request[1]
        print ("%s wrote:" % self.client_address[0]),
        print data,
        data = data.lower()

        # if data starts with o, opened door validation, else should
        # be a code
        if not data.startswith("o"):
            valide, contents = check_code(data)
            if valide:
                socket.sendto("passoir,o=1", self.client_address)
                print "valide ! (%s)" % contents.strip()
            else:
                print
        else:
            print

if __name__ == "__main__":
    server = SocketServer.UDPServer((HOST, PORT), VigileHandler)
    server.serve_forever()
             
